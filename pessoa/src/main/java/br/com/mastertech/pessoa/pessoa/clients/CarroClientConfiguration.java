package br.com.mastertech.pessoa.pessoa.clients;

import feign.Feign;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CarroClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new CarroClientDecoder();
    }



}
